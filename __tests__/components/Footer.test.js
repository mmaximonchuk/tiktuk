import React from "react";
import { ThemeProvider } from "styled-components";

import { render } from "../test-utils";
import Footer from "../../layout/Footer/Footer";

import { myDefaultTheme } from '../../styles/theme';

describe("FooterComponent", () => {
  it("should render the Footer info", () => {
    const copyrights = "© Test Task Limited™, 2020. All rights reserved.";
    const reminder = "Don’t Forget To Deploy Project To GitHub Pages";

    const { getByText } = render(<ThemeProvider theme={myDefaultTheme}><Footer /></ThemeProvider>);

    const reminderNode = getByText(reminder);
    const copyrightsNode = getByText(copyrights);

    expect(reminderNode).toBeVisible();
    expect(copyrightsNode).toBeVisible();
  });
});
