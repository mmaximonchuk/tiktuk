import React from "react";
// import { useRouter } from "next/router";
import { ThemeProvider } from "styled-components";

import { render } from "../test-utils";
// import userEvent from '@testing-library/user-event';
import HomePage from '../../pages/index';

import { myDefaultTheme } from "../../styles/theme";

describe("HomePage", () => {
  it("should render HomePage without crash", () => {
    const { getByText } = render(
        <ThemeProvider theme={myDefaultTheme}>
            <HomePage/>
        </ThemeProvider>
    );

    const heading = getByText('TikTok Feeds');

    expect(heading).toBeVisible();
  });
});
