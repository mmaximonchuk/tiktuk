import { requestVideoData } from '../api/requestVideoData';

import { iVideo } from './../domain/HomePage.interface';
import { iCustomError } from '../domain/HomePage.interface';

export const paginatePosts = async (limit: number, setIsFetching: React.Dispatch<React.SetStateAction<boolean>>, setVideoData: React.Dispatch<React.SetStateAction<iVideo[]>>, setErrorMessage: React.Dispatch<React.SetStateAction<iCustomError>>): Promise<void> => {
	setIsFetching(true);
	setVideoData([]);
	try {
		const data = await requestVideoData(limit);

		setVideoData(data);
		setErrorMessage(null);
	} catch (e) {
		setErrorMessage({
			message: `Some error occurred while pagingating with code ${e}`,
		});
	} finally {
		setIsFetching(false);
	}
};
