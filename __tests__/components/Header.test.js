import React from "react";
import { ThemeProvider } from "styled-components";

import { render } from "../test-utils";
import Header from '../../layout/Header/Header';

import { myDefaultTheme } from "../../styles/theme";

describe("HeaderComponent", () => {
  it("should render the Header heading", () => {
    const { getByText } = render(<ThemeProvider theme={myDefaultTheme}><Header /></ThemeProvider>);
    const appName = "TikTuk :";

    const heading = getByText(appName);

    expect(heading).toBeVisible();
  });
});
