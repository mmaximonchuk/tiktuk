import React, { useState } from 'react';
import type { AppProps } from "next/app";
import { ThemeProvider } from 'styled-components';

import { AppThemes, ThemeContextProvider } from '../store/themeContext';

import { MyGlobalStyle } from '../styles/global';
import { myDefaultTheme, myDarkTheme } from '../styles/theme';
// import "../styles/globals.css";

function MyApp({ Component, pageProps }: AppProps) {
  const [appTheme, setAppTheme] = useState<AppThemes>(AppThemes.Light);

  return <ThemeContextProvider>
    <ThemeProvider theme={appTheme === AppThemes.Light ? myDefaultTheme : myDarkTheme}>
      <MyGlobalStyle />
      <Component {...pageProps} setAppTheme={setAppTheme} appTheme={appTheme} />
    </ThemeProvider>
  </ThemeContextProvider >;
}
export default MyApp;
