import styled, { createGlobalStyle, css } from 'styled-components';


// theme is also fully typed
export const MyGlobalStyle = createGlobalStyle`
  html,
  body {
	margin: 0;
	padding: 0;

	background-color: ${props => props.theme.colors.textWhiteColor};

	font-family: 'Montserrat', sans-serif;
  }
  a {
    text-decoration: none;

    color: inherit;
  }

  * {
    box-sizing: border-box;
  }
`;

// and this theme is fully typed as well
// export const cssHelper = css`
//   border: 1px solid ${props => props.theme.borderRadius};
// `;