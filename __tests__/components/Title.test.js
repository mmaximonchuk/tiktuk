import React from "react";
import { ThemeProvider } from "styled-components";

import { render } from "../test-utils";
import { Title } from "../../components";
import { TitleTags } from "../../domain/Title.props";

import { myDefaultTheme } from "../../styles/theme";

describe("Title", () => {
  it("should render the h1 tag", () => {
    const text = "h1 tag rendered";

    const { getByText } = render(<ThemeProvider theme={myDefaultTheme}><Title tag={TitleTags.h1}>{text}</Title></ThemeProvider>);
    const h1Element = getByText(text);

    expect(h1Element).toBeVisible();
  });

  it("should render the h2 tag", () => {
    const text = "h2 tag rendered";

    const { getByText } = render(<ThemeProvider theme={myDefaultTheme}><Title tag={TitleTags.h2}>{text}</Title></ThemeProvider>);
    const h2Element = getByText(text);

    expect(h2Element).toBeVisible();
  });
	
  it("should render the h3 tag", () => {
    const text = "h3 tag rendered";

    const { getByText } = render(<ThemeProvider theme={myDefaultTheme}><Title tag={TitleTags.h3}>{text}</Title></ThemeProvider>);
    const h3Element = getByText(text);

    expect(h3Element).toBeVisible();
  });

  it("should render the h4 tag", () => {
    const text = "h4 tag rendered";

    const { getByText } = render(<ThemeProvider theme={myDefaultTheme}><Title tag={TitleTags.h4}>{text}</Title></ThemeProvider>);
    const h4Element = getByText(text);

    expect(h4Element).toBeVisible();
  });
});
