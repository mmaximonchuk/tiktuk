import React from "react";
import { render, screen } from "../test-utils";
import HomePage from "../../pages/index";
import { withLayout } from '../../layout/Layout';

describe("HomePage", () => {
  it("should render the heading", () => {
    const textToFind = "TikTok Feeds";
		const HomePageWithLayout = withLayout(HomePage);
		
    render(<HomePageWithLayout />);
    const heading = screen.getByText(textToFind);

    expect(heading).toBeInTheDocument();
  });
});
