import React from "react";
import styled from "styled-components";
import NavLink from "next/link";
import { Typography } from "../../components";
import { TypographyTags } from "../../domain/Typography.props";

interface FooterProps {}

export default function Footer({}: FooterProps): JSX.Element {
  return (
    <StyledFooter>
      <div className={"footerInner"}>
        <Typography tag={TypographyTags.p} className={"footerReminder"}>
          Don’t Forget To Deploy Project To GitHub Pages
        </Typography>
        <Typography tag={TypographyTags.p} className={"rights"}>
          © Test Task Limited™, 2020. All rights reserved.
        </Typography>
        <div className={"footerLinks"}>
          <NavLink href="/">
            <a className={"footerLink"}>
              <Typography tag={TypographyTags.span}>Terms Of Service |</Typography>
            </a>
          </NavLink>
          <NavLink href="/">
            <a className={"footerLink"}>
              <Typography tag={TypographyTags.span}> Privacy Policy</Typography>
            </a>
          </NavLink>
        </div>
      </div>
    </StyledFooter>
  );
}

const StyledFooter = styled.footer`
  display: flex;

  .footerInner {
    max-width: 655px;
    margin: 0 auto;
    padding: 34px 10px;

    text-align: center;
  }

  .footerReminder {
    margin: 15px 0;

    font-size: 27px;
    font-weight: 200;
    line-height: 110%;
  }

  .rights,
  .footerLink {
    font-size: 12px;
    line-height: 1.8;
  }

  .rights {
    font-weight: 600;
  }
`;
