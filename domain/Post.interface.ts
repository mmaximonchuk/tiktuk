import { DetailedHTMLProps, HTMLAttributes } from 'react';
import { iVideo } from './HomePage.interface';

export interface iPostProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>, Omit<iVideo, 'id'> {
	isOnUserPage?: boolean;
}