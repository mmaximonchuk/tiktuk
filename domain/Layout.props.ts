import { DetailedHTMLProps, HTMLAttributes } from 'react';
import { AppThemes } from '../store/themeContext';
export interface LayoutProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	appTheme: AppThemes;
	setAppTheme: React.Dispatch<React.SetStateAction<AppThemes>>;
}