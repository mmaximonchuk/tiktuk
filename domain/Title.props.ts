import { DetailedHTMLProps, HTMLAttributes } from "react";

export interface TitleProps {
	tag: TitleTags;
	className: string;
}

export enum TitleTags {
	h1 = "h1",
	h2 = "h2",
	h3 = "h3",
	h4 = "h4",
}