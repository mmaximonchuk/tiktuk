import React, { PropsWithChildren } from 'react';
// import cn from "classnames";
import styled, { css } from 'styled-components';
import { TypographyProps } from "../../domain/Typography.props";
import { TypographyTags } from '../../domain/Typography.props';


export function Typography({
  tag,
  children,
  className,
  ...restProps
}: PropsWithChildren<TypographyProps>): JSX.Element {

  switch (tag) {
    case TypographyTags.p:
      return (
        <StyledP className={className} {...restProps}>
          {children}
        </StyledP>
      );
    case TypographyTags.span:
      return (
        <StyledSpan className={className} {...restProps}>
          {children}
        </StyledSpan>
      );
    default:
      return <></>;
  }
}

const common = css`
color: ${props => props.theme.colors.textPrimaryColor};
`

const StyledP = styled.p`
	margin: 0;
  ${common}
`

const StyledSpan = styled.span`
  color: var(--text-primary-color--light);
  ${common}
`
