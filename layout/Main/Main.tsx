import React from "react";
import styled from "styled-components";

import { MainProps } from "../../domain/Main.props";

export default function Main({ children, className }: MainProps): JSX.Element {
  return <StyledMain className={className}>{children}</StyledMain>;
}

const StyledMain = styled.main`
	display: flex;
	flex: 1;

	width: 100vw;
	max-width: 1231px;
	margin: 0 auto;
	padding: 30px 10px 0;

`