import axios from "axios";
import { iVideo } from "../domain/HomePage.interface";
import { iUser } from "../domain/User.interface";
import { apiRequestHeaders } from '../helpers/apiHeders';

export const VideosAPI = {
  requestVideoData: async (limit: number): Promise<iVideo[]> => {
    const { data } = await axios.request<iVideo[]>({
      method: "GET",
      url: `https://${process.env.NEXT_PUBLIC_RAPID_HOST}/trending/feed?limit=${limit}`,
      headers: apiRequestHeaders,
    });
    return data;
  },
  requestUserInfo: async (username: string): Promise<iUser> => {
    const { data: userInfo } = await axios.request<iUser>({
      method: "GET",
      url: `https://${process.env.NEXT_PUBLIC_RAPID_HOST}/user/info/${username}`,
      headers: apiRequestHeaders,
    });
    return userInfo;
  },
};
