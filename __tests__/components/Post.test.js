import { ThemeProvider } from "styled-components";

import { render } from "../test-utils";
import { Post } from "../../components";

import { myDefaultTheme } from "../../styles/theme";

describe("PostComponent", () => {
  let postProps;

  beforeEach(() => {
    postProps = {
      id: "7030830332291058949",
      desc: "I think Marsiglia is mad at me 😱 💰 😂Casa de Papel or Squid Game? Which one do you prefer?@Luka Peros #leanfromkhaby #marsiglia#casadepapel #squid",
      createTime: 1636992753,
      video: {
        id: "7030830332291058949",
        height: 1024,
        width: 576,
        duration: 43,
        ratio: "720p",
        cover:
          "https://p16-sign-va.tiktokcdn.com/obj/tos-maliva-p-0068/8fd0621aad2448448e964261934c92ac_1636992763?x-expires=1637024400&x-signature=gDDq1te1mWGuHqpRIHRR9Lwg%2FeE%3D",
        originCover:
          "https://p16-sign-va.tiktokcdn.com/obj/tos-maliva-p-0068/dddb9de7674b48b5ade6b98cba3716f7_1636992756?x-expires=1637024400&x-signature=naRJ2%2Bm%2FqZf2svOnzYugEmxEinc%3D",
        dynamicCover:
          "https://p16-sign-va.tiktokcdn.com/obj/tos-maliva-p-0068/f18f30746ca642d68cd5b8e9e9ace3f7_1636992757?x-expires=1637024400&x-signature=eXu8aVPE32E7aEHuWZ04rNlf2IM%3D",
        playAddr:
          "https://v16-web.tiktok.com/video/tos/useast2a/tos-useast2a-ve-0068c003/908b5e8c6a4a46b688eaab6f15a21d51/?a=1988&br=2890&bt=1445&cd=0%7C0%7C1&ch=0&cr=0&cs=0&cv=1&dr=0&ds=3&er=&expire=1637026916&ft=wUyFfF5qkag3-I&l=202111151941130101902192204E355225&lr=tiktok_m&mime_type=video_mp4&net=0&pl=0&policy=3&qs=0&rc=ajk0NjU6Zjw7OTMzNzczM0ApaGU8aGZmaGVoNzY3NjNoZmdhaGZncjRfNjZgLS1kMTZzczBeLl8wYDAyNWMxLmIxX2E6Yw%3D%3D&signature=1ce62eb5be7681527214da1353eb50da&tk=0&vl=&vr=",
        downloadAddr:
          "https://v16-web.tiktok.com/video/tos/useast2a/tos-useast2a-ve-0068c003/908b5e8c6a4a46b688eaab6f15a21d51/?a=1988&br=2890&bt=1445&cd=0%7C0%7C1&ch=0&cr=0&cs=0&cv=1&dr=0&ds=3&er=&expire=1637026916&ft=wUyFfF5qkag3-I&l=202111151941130101902192204E355225&lr=tiktok_m&mime_type=video_mp4&net=0&pl=0&policy=3&qs=0&rc=ajk0NjU6Zjw7OTMzNzczM0ApaGU8aGZmaGVoNzY3NjNoZmdhaGZncjRfNjZgLS1kMTZzczBeLl8wYDAyNWMxLmIxX2E6Yw%3D%3D&signature=1ce62eb5be7681527214da1353eb50da&tk=0&vl=&vr=",
        shareCover: [
          "",
          "https://p16-sign-va.tiktokcdn.com/tos-maliva-p-0068/dddb9de7674b48b5ade6b98cba3716f7_1636992756~tplv-tiktok-play.jpeg?x-expires=1637024400&x-signature=KYG4xdcJgSKGmeX0Ps2XIy8MXPM%3D",
          "https://p16-sign-va.tiktokcdn.com/tos-maliva-p-0068/dddb9de7674b48b5ade6b98cba3716f7_1636992756~tplv-tiktokx-share-play.jpeg?x-expires=1637024400&x-signature=klZqR8yQHBuR7XnedJ3inYQSSb4%3D",
        ],
        reflowCover:
          "https://p16-sign-va.tiktokcdn.com/obj/tos-maliva-p-0068/8fd0621aad2448448e964261934c92ac_1636992763?x-expires=1637024400&x-signature=gDDq1te1mWGuHqpRIHRR9Lwg%2FeE%3D",
        bitrate: 1480109,
        encodedType: "normal",
        format: "mp4",
        videoQuality: "normal",
        encodeUserTag: "",
        codecType: "h264",
        definition: "720p",
      },
      author: {
        id: "127905465618821121",
        uniqueId: "khaby.lame",
        nickname: "Khabane lame",
        avatarThumb:
          "https://p16-sign-va.tiktokcdn.com/tos-maliva-avt-0068/30107337091e90a33da10d6ce1815bb1~c5_100x100.jpeg?x-expires=1637089200&x-signature=XmM8EKU3dRX6%2BJjkam0Pb9KsM3g%3D",
        avatarMedium:
          "https://p16-sign-va.tiktokcdn.com/tos-maliva-avt-0068/30107337091e90a33da10d6ce1815bb1~c5_720x720.jpeg?x-expires=1637089200&x-signature=pP3CywqcTimYHOWF2Mw8UA98Wyw%3D",
        avatarLarger:
          "https://p16-sign-va.tiktokcdn.com/tos-maliva-avt-0068/30107337091e90a33da10d6ce1815bb1~c5_1080x1080.jpeg?x-expires=1637089200&x-signature=xZGEthXVaaa8bM3X1%2Fqh8f7PJBA%3D",
        signature: "Se vuoi ridere sei nel posto giusto😎 \nIf u wanna laugh u r in the right place😎",
        verified: true,
        secUid: "MS4wLjABAAAAwAg0rSzO65WQfz4RzQgGv2Xdv108BgPXhRrrmNVIHQZ9PO8-flwwRtEppYTS0OjA",
        secret: false,
        ftc: false,
        relation: 0,
        openFavorite: false,
        commentSetting: 0,
        duetSetting: 0,
        stitchSetting: 0,
        privateAccount: false,
      },
      music: {
        id: "7030830200678025989",
        title: "suono originale",
        playUrl: "https://sf16-ies-music-va.tiktokcdn.com/obj/musically-maliva-obj/7030830268755757829.mp3",
        coverThumb:
          "https://p16-sign-va.tiktokcdn.com/tos-maliva-avt-0068/30107337091e90a33da10d6ce1815bb1~c5_100x100.jpeg?x-expires=1637089200&x-signature=XmM8EKU3dRX6%2BJjkam0Pb9KsM3g%3D",
        coverMedium:
          "https://p16-sign-va.tiktokcdn.com/tos-maliva-avt-0068/30107337091e90a33da10d6ce1815bb1~c5_720x720.jpeg?x-expires=1637089200&x-signature=pP3CywqcTimYHOWF2Mw8UA98Wyw%3D",
        coverLarge:
          "https://p16-sign-va.tiktokcdn.com/tos-maliva-avt-0068/30107337091e90a33da10d6ce1815bb1~c5_1080x1080.jpeg?x-expires=1637089200&x-signature=xZGEthXVaaa8bM3X1%2Fqh8f7PJBA%3D",
        authorName: "Khabane lame",
        original: true,
        duration: 43,
        album: "",
      },
      challenges: [
        {
          id: "1698240028851205",
          title: "leanfromkhaby",
          desc: "",
          profileThumb: "",
          profileMedium: "",
          profileLarger: "",
          coverThumb: "",
          coverMedium: "",
          coverLarger: "",
          isCommerce: false,
        },
        {
          id: "36503609",
          title: "marsiglia",
          desc: "",
          profileThumb: "",
          profileMedium: "",
          profileLarger: "",
          coverThumb: "",
          coverMedium: "",
          coverLarger: "",
          isCommerce: false,
        },
        {
          id: "81491010",
          title: "casadepapel",
          desc: "",
          profileThumb: "",
          profileMedium: "",
          profileLarger: "",
          coverThumb: "",
          coverMedium: "",
          coverLarger: "",
          isCommerce: false,
        },
        {
          id: "157399",
          title: "squid",
          desc: "",
          profileThumb: "",
          profileMedium: "",
          profileLarger: "",
          coverThumb: "",
          coverMedium: "",
          coverLarger: "",
          isCommerce: false,
        },
      ],
      stats: {
        diggCount: 305500,
        shareCount: 1187,
        commentCount: 7347,
        playCount: 2000000,
      },
      duetInfo: {
        duetFromId: "0",
      },
      originalItem: false,
      officalItem: false,
      textExtra: [
        {
          awemeId: "",
          start: 94,
          end: 105,
          hashtagName: "",
          hashtagId: "",
          type: 0,
          userId: "7013409279916164101",
          isCommerce: false,
          userUniqueId: "reallukaperos",
          secUid: "MS4wLjABAAAAzaPmoaFIOvgPXU5QwMWalg4RuDbqO3hrfviZmX-3swYcId7gdwSnk9wLbWKtIxqf",
          subType: 0,
        },
        {
          awemeId: "",
          start: 106,
          end: 120,
          hashtagName: "leanfromkhaby",
          hashtagId: "1698240028851205",
          type: 1,
          userId: "",
          isCommerce: false,
          userUniqueId: "",
          secUid: "",
          subType: 0,
        },
        {
          awemeId: "",
          start: 121,
          end: 131,
          hashtagName: "marsiglia",
          hashtagId: "36503609",
          type: 1,
          userId: "",
          isCommerce: false,
          userUniqueId: "",
          secUid: "",
          subType: 0,
        },
        {
          awemeId: "",
          start: 131,
          end: 143,
          hashtagName: "casadepapel",
          hashtagId: "81491010",
          type: 1,
          userId: "",
          isCommerce: false,
          userUniqueId: "",
          secUid: "",
          subType: 0,
        },
        {
          awemeId: "",
          start: 144,
          end: 150,
          hashtagName: "squid",
          hashtagId: "157399",
          type: 1,
          userId: "",
          isCommerce: false,
          userUniqueId: "",
          secUid: "",
          subType: 0,
        },
      ],
      secret: false,
      forFriend: false,
      digged: false,
      itemCommentStatus: 0,
      showNotPass: false,
      vl1: false,
      itemMute: false,
      authorStats: {
        followingCount: 63,
        followerCount: 120500000,
        heartCount: 1900000000,
        videoCount: 971,
        diggCount: 5362,
        heart: 1900000000,
      },
      privateItem: false,
      duetEnabled: false,
      stitchEnabled: false,
      shareEnabled: true,
      isAd: false,
      duetDisplay: 0,
      stitchDisplay: 0,
    };
  });

  test("should render propper PostCard on indexPage", () => {
    const { getByText, getByTestId, queryByText } = render(<ThemeProvider theme={myDefaultTheme}><Post {...postProps} /></ThemeProvider>);
    const nickname = getByText(postProps.author.nickname);
    const desc = getByText(postProps.desc);
    const playCount = getByText(`Comments: ${postProps.stats.playCount}`);
    const diggCount = getByText(`Likes: ${postProps.stats.diggCount}`);
    const video = getByTestId("video-post-element");
    const totalViews = queryByText(`Total Views: ${postProps.stats.playCount}`);

    expect(nickname).toBeVisible();
    expect(desc).toBeVisible();
    expect(playCount).toBeVisible();
    expect(diggCount).toBeVisible();
    expect(video).toBeVisible();

    expect(totalViews).toBeNull();
  });

  test("should render propper PostCard on userPage", () => {
    const { getByText, getByTestId, queryByText } = render(<ThemeProvider theme={myDefaultTheme}><Post {...postProps} isOnUserPage/></ThemeProvider>);
    const desc = getByText(postProps.desc);
    const totalViews = getByText(`Total Views: ${postProps.stats.playCount}`);
    const video = getByTestId("video-post-element");
    const playCount = queryByText(`Comments: ${postProps.stats.playCount}`);
    const diggCount = queryByText(`Likes: ${postProps.stats.diggCount}`);

    expect(desc).toBeVisible();
    expect(totalViews).toBeVisible();
    expect(video).toBeVisible();
    
    expect(playCount).toBeNull();
    expect(diggCount).toBeNull();
  });
  
});
