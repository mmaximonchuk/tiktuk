import React, { PropsWithChildren } from "react";
import styled, { css } from "styled-components";
import { TitleProps, TitleTags } from "../../domain/Title.props";

export function Title({
  tag,
  children,
  className,
  ...restProps
}: PropsWithChildren<TitleProps>): JSX.Element {
  switch (tag) {
    case TitleTags.h1:
      return (
        <StyledH1 className={className} {...restProps}>
          {children}
        </StyledH1>
      );
    case TitleTags.h2:
      return (
        <StyledH2 className={className} {...restProps}>
          {children}
        </StyledH2>
      );
    case TitleTags.h3:
      return (
        <StyledH3 className={className} {...restProps}>
          {children}
        </StyledH3>
      );
    case TitleTags.h4:
      return (
        <StyledH4 className={className} {...restProps}>
          {children}
        </StyledH4>
      );
    default:
      return <></>;
  }
}


const commonTitleStyles = css`
margin: 0;
padding: 0;

color: ${props => props.theme.colors.textPrimaryColor};

`

const StyledH1 = styled.h1`
${commonTitleStyles}

`
const StyledH2 = styled.h1`
${commonTitleStyles}
`
const StyledH3 = styled.h1`
${commonTitleStyles}
`
const StyledH4 = styled.h1`
${commonTitleStyles}
`