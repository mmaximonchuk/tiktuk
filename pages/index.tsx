import React, { useEffect, useRef, useContext } from "react";
import type { GetStaticProps } from "next";
import { ParsedUrlQuery } from "querystring";
// import Image from "next/image";
import Head from "next/head";
// import Link from "next/link";
import styled from 'styled-components';
import { useRouter } from "next/router";

import { Post, Skeletons, Title, Typography } from "../components";
import { withLayout } from "../layout/Layout";
import { TypographyTags } from "../domain/Typography.props";
import { TitleTags } from "../domain/Title.props";
import { paginatePosts } from "../services/paginatePostsService";
import { FeedsContext } from "../store/feedsContext";
import { mockUserFeed } from '../helpers/mocksData';

import { iCustomError, iVideo } from "../domain/HomePage.interface";

import { VideosAPI } from "../api/videos";


const getStaticProps: GetStaticProps<HomeProps, ParsedUrlQuery> = async (ctx) => {
  const limit = Number.parseInt(process.env.NEXT_PUBLIC_POSTS_PER_PAGE_LIMIT) || 30;

  try {
    const data = await VideosAPI.requestVideoData(limit);
    console.log("data: ", data);

    return {
      props: { data },
      revalidate: 1,
    };
  } catch (e) {
    const error = {
      message: `Some error ocuured ${e}`,
    };
    return {
      props: { error },
      revalidate: 1,
    };
  }
};


const HomePage = ({ }: HomeProps): JSX.Element => {
  const router = useRouter();
  const elementScrollToRef = useRef<HTMLDivElement>();
  const {
    data,
    setVideoData,
    errorMessage,
    setErrorMessage,
    isFetching,
    setIsFetching,
    handlePaginate,
  } = useContext(FeedsContext);

  const limitPerPage: string = process.env.NEXT_PUBLIC_POSTS_PER_PAGE_LIMIT || "30";
  
  useEffect(() => {
    // setVideoData(data);
    setIsFetching(true);
    setTimeout(() => {
      setVideoData(mockUserFeed);
      setIsFetching(false);
    }, 2000)
  }, []);

  useEffect(() => {
    if(router?.query?.page) {
      const cPage = router.query.page;
      let limit: number = Array.isArray(cPage)
      ? Number.parseInt(cPage[0]) * Number.parseInt(limitPerPage)
      : Number.parseInt(cPage) * Number.parseInt(limitPerPage);

      paginatePosts(limit, setIsFetching, setVideoData, setErrorMessage);  
    }

  }, [router?.query]);

  useEffect(() => {
    if (elementScrollToRef?.current) {
      elementScrollToRef.current.scrollIntoView();
    }
  }, [isFetching]);

  const showVideoData = typeof window !== "undefined" && Array.isArray(data) && data?.length > 0;
  const showError = typeof window !== "undefined" && errorMessage?.message?.length;
  const showSkeleton = typeof window !== "undefined" && isFetching;

  return (
    <StyledHome>
      <Head>
        <title>Test Task</title>
        <meta name="description" content="TikTuk :) videos" />
        <meta property="og:title" content="Test Task" />
        <meta property="og:description" content="Just click and relax!" />
        <meta property="og:type" content="article" />
      </Head>

      <div className={'firstSection'}>
        <Title tag={TitleTags.h1} className={'firstSectionTitle'}>
          TikTok Feeds
        </Title>

        {showVideoData &&
          data.map((video: iVideo): JSX.Element => {
            return (
              <Post key={video.id} {...video} />
            );
          })}

        {showError && (
          <div className={'error'}>
            <Typography tag={TypographyTags.p}>
              {errorMessage.message}, please wait some minutes and reload a page
            </Typography>
          </div>
        )}
        {showSkeleton && <Skeletons />}
      </div>
      <div className={'paginations'}>
        {[1, 2, 3, 4].map((p) => (
          <button key={p} className={'pager'} onClick={() => handlePaginate(p, router)}>
            {p}
          </button>
        ))}
      </div>
    </StyledHome>
  );
};



interface HomeProps extends Record<string, unknown> {
  data?: iVideo[];
  error?: iCustomError;
}

export default withLayout(HomePage);


const StyledHome = styled.section`
width: 100%;
text-align: center;  
.title {
	margin-bottom: 30px;

	transform: translateX(54px);

	font-size: 58px;
	line-height: 1.1;
}

.error {
	padding: 10px;

	border: 1px solid red;

	font-size: 24px;
}

.paginations {
	display: flex;
	align-items: center;
	flex-wrap: wrap;
	justify-content: center;

	width: 100%;
	max-width: 500px;
	margin: 10px auto 30px;
}

.pager {
	all: unset;

	margin: 0 8px 8px 8px;
	padding: 5px 10px;

	cursor: pointer;

	background-color: ${props => props.theme.colors.black400};

	font-size: 16px;
}

.firstSectionTitle {
	margin-bottom: 30px;

	text-align: center;

	font-size: 33px;
	font-weight: 700;
	line-height: 47px;
}


`