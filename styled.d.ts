import 'styled-components';

// and extend them!
declare module 'styled-components' {
	export interface DefaultTheme {
		colors: {
				primary: string;
				primary100: string;
				orange: string;
				blue: string;
				blue900: string;
				textWhiteColor: string;
				textPrimaryColor: string;
				black: string;
				black025: string;
				black400: string;
				black900: string;
				black006: string;
				black008: string;
		}
	}
	export interface DarkTheme {
		colors: {
			primary: string;
			primary100: string;
			orange: string;
			blue: string;
			blue900: string;
			textWhiteColor: string;
			textPrimaryColor: string;
			black: string;
			black025: string;
			black400: string;
			black900: string;
			black006: string;
			black008: string;
		}
	}

}
	// export interface DefaultTheme {
	// 	colors: {
	// 		light: {
	// 			primary: string;
	// 			primary100: string;
	// 			orange: string;
	// 			blue: string;
	// 			blue900: string;
	// 			textWhiteColor: string;
	// 			textPrimaryColor: string;
	// 			black: string;
	// 			black025: string;
	// 			black400: string;
	// 			black900: string;
	// 			black006: string;
	// 			black008: string;
	// 		},
	// 		dark: {
	// 			primary: string;
	// 			primary100: string;
	// 			orange: string;
	// 			blue: string;
	// 			blue900: string;
	// 			textWhiteColor: string;
	// 			textPrimaryColor: string;
	// 			black: string;
	// 			black025: string;
	// 			black400: string;
	// 			black900: string;
	// 			black006: string;
	// 			black008: string;
	// 		}
	// 	};
	// }
	// export interface DarkTheme {
	// 	colors: {
	// 		light: {
	// 			primary: string;
	// 			primary100: string;
	// 			orange: string;
	// 			blue: string;
	// 			blue900: string;
	// 			textWhiteColor: string;
	// 			textPrimaryColor: string;
	// 			black: string;
	// 			black025: string;
	// 			black400: string;
	// 			black900: string;
	// 			black006: string;
	// 			black008: string;
	// 		},
	// 		dark: {
	// 			primary: string;
	// 			primary100: string;
	// 			orange: string;
	// 			blue: string;
	// 			blue900: string;
	// 			textWhiteColor: string;
	// 			textPrimaryColor: string;
	// 			black: string;
	// 			black025: string;
	// 			black400: string;
	// 			black900: string;
	// 			black006: string;
	// 			black008: string;
	// 		}
	// 	};
	// }
