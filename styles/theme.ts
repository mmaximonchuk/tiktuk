import { DefaultTheme, DarkTheme } from 'styled-components';

const myDefaultTheme: DefaultTheme = {
	colors: {
		primary: '#431BA1',
		primary100: '#6F44E6',
		orange: '#FE805C',
		blue: 'blue',
		blue900: 'rgb(45, 0, 151)',
		textWhiteColor: '#FBFBFB',
		textPrimaryColor: '#212353',
		black: '#000000',
		black025: 'rgba(0, 0, 0, 0.25)',
		black400: 'rgba(0, 0, 0, 0.4)',
		black900: 'rgba(0, 0, 0, 0.9)',
		black006: 'rgba(0, 0, 0, 0.06)',
		black008: 'rgba(0, 0, 0, 0.08)',
	},
};

const myDarkTheme: DarkTheme = {
	colors: {
		primary: 'rgb(216, 216, 216)',
		primary100: '#F8921D',
		orange: '#C658F1',
		blue: 'rgb(216, 158, 255)',
		blue900: 'rgb(216, 158, 255)',
		textWhiteColor: '#19015c',
		textPrimaryColor: '#ABABBB',
		black: 'rgb(255, 255, 255)',
		black025: 'rgba(255, 255, 255, 0.25)',
		black400: 'rgba(255, 255, 255, 0.4)',
		black900: 'rgba(255, 255, 255, 0.9)',
		black006: 'rgba(255, 255, 255, 0.06)',
		black008: 'rgba(255, 255, 255, 0.08)',
	},
};
export { myDefaultTheme, myDarkTheme };