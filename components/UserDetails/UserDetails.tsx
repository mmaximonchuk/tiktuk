import React from "react";
import Image from "next/image";

import { Typography } from "..";
import { TypographyTags } from "../../domain/Typography.props";
import { iUserDetailsProps } from "../../domain/UserDetails.interface";

import iconVerified from "../../assets/images/homePage/verify.png";
import styled from "styled-components";

// import s from "../../styles/User.module.scss";

export const UserDetails = ({ stats, user }: iUserDetailsProps): JSX.Element => {
  const isAtServerSide = typeof window !== "undefined";
  const showBage = isAtServerSide && user.verified && iconVerified !== undefined;
  
  return (
    <>
      <StyledUserDetails className={'info'}>
        <div className={'avatarWrapper'}>
          <Image
            className={'avatar'}
            src={user.avatarLarger}
            layout="fixed"
            alt={user.uniqueId}
            width={100}
            height={100}
          />
          <Typography tag={TypographyTags.p} className={'userName'}>
            {user.nickname}
            {showBage ? <img src={"../../assets/images/homePage/verify.png"} alt="user is verified" width={32} height={32} /> : null}
          </Typography>
        </div>
        <Typography tag={TypographyTags.p} className={'signature'}>
          {user.signature}
        </Typography>
      </StyledUserDetails>
      <div className={'statsWrapper'}>
        <Typography tag={TypographyTags.p} className={'stats'}>
          Followers: {stats.followerCount}
        </Typography>
        <Typography tag={TypographyTags.p} className={'stats'}>
          Following: {stats.followingCount}
        </Typography>
        <Typography tag={TypographyTags.p} className={'stats'}>
          Hearts: {stats.heartCount}
        </Typography>
        <Typography tag={TypographyTags.p} className={'stats'}>
          Videos: {stats.videoCount}
        </Typography>
      </div>
    </>
  );
};


const StyledUserDetails = styled.div`

  display: flex;
  flex-direction: column;

  margin-bottom: 20px;

.avatarWrapper {
  display: flex;
  align-items: center;

  margin-bottom: 25px;
}

.avatar {
}

.userName {
  display: flex;
  align-items: center;

  margin-left: 20px;

  font-size: 28px;
}

.verificationIcon {
  display: inline-flex;

  margin-left: 10px;
}

.signature {
  font-size: 20px;
  font-weight: 700;
}

.statsWrapper {
  margin-bottom: 50px;
}

.stats {
  display: flex;

  margin-bottom: 8px;

  color: var(--dark--light);

  font-size: 18px;
  font-weight: 500;
}

`