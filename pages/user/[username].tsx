import React, { useContext, useEffect } from "react";
// import Image from "next/image";
import styled from 'styled-components';
import { iVideo } from "../../domain/HomePage.interface";
import { iUser } from "../../domain/User.interface";

import { withLayout } from "../../layout/Layout";
import { Post, Skeletons, Typography, UserDetails } from "../../components";
import { TypographyTags } from "../../domain/Typography.props";
import { UserContext } from "../../store/userContext";
import { mockUserInfo } from "../../helpers/mocksData";

import { VideosAPI } from "../../api/videos";

// import homeStyles from "../../styles/Home.module.scss";
// import s from "../../styles/User.module.scss";

function User({ userInfo }: iUserProps): JSX.Element {
  const { setUser, userFeed, user, error } = useContext(UserContext);

  useEffect(() => {
    // setUser(userInfo);
    setTimeout(() => {
      setUser(mockUserInfo);
    });
  }, [userInfo]);

  const isArrayAtServerSide = typeof window !== "undefined" && Array.isArray(userFeed);
  const showUserData = typeof window !== "undefined" && userInfo?.user && userInfo?.stats;
  const showUserFeed = isArrayAtServerSide && Boolean(userFeed?.length);
  const showSkeleton = isArrayAtServerSide && !userFeed?.length;
  const showErrorWithUserFeed = typeof window !== "undefined" && !Array.isArray(userFeed);
  const showHTTPSError = typeof window !== "undefined" && error?.message?.length;

  return (
    <StyledUserPage className={'container'}>
      <div className={'userContainer'}>
        {showUserData && (
          <UserDetails {...user} />
        )}
      </div>

      {showUserFeed && (
        <div>
          {userFeed.map((video: iVideo) => {
            return (
              <Post key={video.id} {...video} isOnUserPage />
            );
          })}
        </div>
      )}
      {showSkeleton && <Skeletons />}
      {showErrorWithUserFeed && (
        <Typography tag={TypographyTags.p} className={'error'}>
          Some error occurred with code: {error?.message}. Please try again and reaload a page!
        </Typography>
      )}
      {showHTTPSError && (
        <Typography tag={TypographyTags.p} className={'error'}>
          Some error occurred: {error.message}. Please try again and reaload a page!
        </Typography>
      )}
    </StyledUserPage>
  );
}

export default withLayout(User);

export async function getStaticPaths() {
  const limit = 100;
  try {
    const data = await VideosAPI.requestVideoData(limit);
    const paths = data.map((video) => ({
      params: { username: video.author.nickname },
    }));

    return { paths, fallback: true };
  } catch (e) {
    console.log(e);
  }
}

export const getStaticProps = async ({ params }) => {
  const username = params.username;
  const limit = Number.parseInt(process.env.PER_PAGE_LIMIT) || 30;

  try {
    const userFeed = await VideosAPI.requestVideoData(limit);
    const userInfo = await VideosAPI.requestUserInfo(username);

    return {
      props: { userFeed, userInfo },
      revalidate: 1,
    };
  } catch (e) {
    const error = {
      message: `Some error ocuured ${e}`,
    };
    return {
      props: { error },
      revalidate: 1,
    };
  }
};

export interface iUserProps extends Record<string, unknown> {
  userInfo?: iUser;
}


const StyledUserPage = styled.section`
	width: 100%;

.videoText {
	margin-bottom: 10px;
}

.comments {
	margin-bottom: 10px;
}

.videoCard {
	padding-top: 20px;
	padding-bottom: 0;
}

.userContainer {
	display: flex;
	flex-direction: column;

	.info {
		display: flex;
		flex-direction: column;

		margin-bottom: 20px;
	}

	.avatarWrapper {
		display: flex;
		align-items: center;

		margin-bottom: 25px;
	}

	.avatar {
	}

	.userName {
		display: flex;
		align-items: center;

		margin-left: 20px;

		font-size: 28px;
	}

	.verificationIcon {
		display: inline-flex;

		margin-left: 10px;
	}

	.signature {
		font-size: 20px;
		font-weight: 700;
	}

	.statsWrapper {
		margin-bottom: 50px;
	}

	.stats {
		display: flex;

		margin-bottom: 8px;

		color: var(--dark--light);

		font-size: 18px;
		font-weight: 500;
	}
}

.error {
	padding: 10px;

	border: 1px solid red;

	font-size: 24px;
}

`