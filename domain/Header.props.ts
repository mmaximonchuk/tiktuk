import { AppThemes } from './../store/themeContext';
import { DetailedHTMLProps, HTMLAttributes } from 'react';
export interface HeaderProps extends DetailedHTMLProps<HTMLAttributes<HTMLHeadElement>, HTMLHeadElement> {
	appTheme: AppThemes;
	setAppTheme: React.Dispatch<React.SetStateAction<AppThemes>>;
}	