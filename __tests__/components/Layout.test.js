import React from "react";
import { ThemeProvider } from "styled-components";

import { render } from "../test-utils";
import {Layout} from "../../layout/Layout";

import { myDefaultTheme } from "../../styles/theme";



describe("Layout", () => {
  it("should render Layout with children", () => {
  const childrenComponent = `Test Children Component`;
    const { getByText } = render(<ThemeProvider theme={myDefaultTheme}><Layout>{childrenComponent}</Layout></ThemeProvider>);

    const childrenNode = getByText(childrenComponent);

    expect(childrenNode).toContainHTML(childrenComponent);
  });
});
