import axios from "axios";
import { iUser } from '../domain/User.interface';
import { apiRequestHeaders } from '../helpers/apiHeders';

export const requestUserInfo = async (username: string): Promise<iUser> => {
	const { data: userInfo } = await axios.request<iUser>({
		method: "GET",
		url: `https://${process.env.NEXT_PUBLIC_RAPID_HOST}/user/info/${username}`,
		headers: apiRequestHeaders,
	});
	return userInfo;
}