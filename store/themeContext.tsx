import React, { createContext, useEffect, useState } from 'react';

export enum AppThemes {
	Dark = "dark",
	Light = "light"
}

export interface iThemeContext {
	theme: AppThemes;
	setTheme: React.Dispatch<React.SetStateAction<AppThemes>>;
}

export const ThemeContext = createContext<iThemeContext>({ theme: AppThemes.Light, setTheme: () => { } });

export const ThemeContextProvider = ({ children }): JSX.Element => {
	const [theme, setTheme] = useState<AppThemes>(AppThemes.Light);

	useEffect(() => {
		const html = document.querySelector("html") as HTMLElement;
		html.className = theme;
	}, [theme]);

	return <ThemeContext.Provider value={{ theme, setTheme }}>{children}</ThemeContext.Provider>
};
