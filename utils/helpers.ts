export const apiRequestHeaders = {
	"x-rapidapi-host": process.env.NEXT_PUBLIC_RAPID_HOST,
	"x-rapidapi-key": process.env.NEXT_PUBLIC_RAPID_KEY,
}