import React from "react";
import styled from "styled-components";

function Skeleton(): JSX.Element {
  return <StyledSkeleton></StyledSkeleton>;
}

export function Skeletons(): JSX.Element {
  const templateSkeletons: number[] = [1, 2, 3, 4];
  return (
    <>
      {templateSkeletons.map((skelet) => (
        <Skeleton key={skelet} />
      ))}
    </>
  );
}

const StyledSkeleton = styled.div`

    width: 100%;
    max-width: 540px;
    height: 250px;
    margin: 0 auto;

    cursor: progress;
    animation: loading 1.5s infinite;

    background: linear-gradient(0.25turn, transparent, #fff, transparent), linear-gradient(#eee, #eee),
      radial-gradient(38px circle at 19px 19px, #eee 50%, transparent 51%), linear-gradient(#eee, #eee);
    background-repeat: no-repeat;
    background-position: -540px 0, 0 0, 0 190px, 50px 195px;
    background-size: 540px 250px, 540px 180px, 100px 100px, 225px 30px;
  }

  @keyframes loading {
    to {
      background-position: 315px 0, 0 0, 0 190px, 50px 195px;
    }
  }
`;
