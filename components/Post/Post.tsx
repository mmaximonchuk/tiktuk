import React, { memo } from "react";
import Link from "next/link";
import Image from "next/image";

import { Typography } from "..";
import { Challenge } from "../../domain/HomePage.interface";
import { TypographyTags } from "../../domain/Typography.props";
import { iPostProps } from "../../domain/Post.interface";

// import homeStyles from "../../styles/Home.module.scss";
// import s from "../../styles/Home.module.scss";
import styled from "styled-components";

export const Post = ({
  video: { width, playAddr, height, cover },
  desc,
  challenges = null,
  author: { avatarLarger, nickname },
  stats: { playCount, diggCount },
  isOnUserPage = false,
}: iPostProps): JSX.Element => {
  return (
    <StyledPost>
      <div className={"videoCard"}>
        <video
          data-testid="video-post-element"
          className={"videoInner"}
          width={width}
          height={height}
          controls
          poster={''}
          // poster={cover}
        >
          {/* <source src={playAddr} type="video/mp4" /> */}
          <source src={''} type="video/mp4" />
          Тег video не поддерживается вашим браузером.
        </video>

        {!isOnUserPage && (
          <Link href={`/user/${nickname}`}>
            <a className={"creator"}>
              <Image
                className={"creatorAvatar"}
                src={avatarLarger}
                alt={nickname}
                layout="fixed"
                width={76}
                height={76}
              />
              <Typography tag={TypographyTags.span} className={"creatorName"}>
                {nickname}
              </Typography>
            </a>
          </Link>
        )}

        <Typography tag={TypographyTags.p} className={"videoText"}>
          {desc}
        </Typography>
        {Boolean(challenges?.length) && (
          <div className={"hashTags"}>
            {challenges.map((tag: Challenge) => {
              return (
                <Typography key={tag.id} tag={TypographyTags.span} data-id={tag.id} className={"hashTag"}>
                  #{tag.title}
                </Typography>
              );
            })}
          </div>
        )}

        {isOnUserPage ? (
          <Typography tag={TypographyTags.p} className={"comments"}>
            Total Views: {playCount}
          </Typography>
        ) : (
          <>
            <Typography tag={TypographyTags.p} className={"comments"}>
              Comments: {playCount}
            </Typography>
            <Typography tag={TypographyTags.p} className={"likes"}>
              Likes: {diggCount}
            </Typography>
          </>
        )}
      </div>
    </StyledPost>
  );
};

const StyledPost = styled.div`
display: flex;
justify-content: center;

max-width: 805px;
margin: 0 auto;

.comments {
	margin-bottom: 10px;
}

.videoCard {
	position: relative;

	display: flex;
	align-items: flex-start;
	flex-direction: column;

	width: 100%;
	max-width: 576px;
	height: auto;
	margin-bottom: 50px;
	padding-bottom: 25px;

	text-align: center;

	color: ${(props) => props.theme.colors.textWhiteColor};
	border-radius: 22px;
	background-color: ${(props) => props.theme.colors.black900};
	filter:
		drop-shadow(0 69.5875px 139.175px ${(props) => props.theme.colors.black008})
		drop-shadow(0 14.65px 43.95px ${(props) => props.theme.colors.black006});
}

.videoInner {
	display: flex;

	width: 100%;
	margin-bottom: 20px;
}

.hashTags {
	display: flex;
	flex-wrap: wrap;

	width: 100%;
	margin-bottom: 15px;
	padding: 0 10px;
}

.hashTag {
	margin-right: 5px;
	margin-bottom: 5px;
	padding: 5px 8px;

	color: ${(props) => props.theme.colors.textWhiteColor};
	border: 1px solid ${(props) => props.theme.colors.blue};
	border-radius: 10px;
	background-color: ${(props) => props.theme.colors.blue900};
}

.videoCard:not(:first-child) {
	margin-left: 28px;
}

.creator,
.videoText {
	color: ${(props) => props.theme.colors.black};
}

.creator {
	position: absolute;
	top: 15px;
	left: 15px;

	display: flex;
	align-items: center;

	margin-top: 10px;
	margin-bottom: 26px;

	font-size: 17px;
	font-weight: 700;
	line-height: 22px;
}

.creatorAvatar {
	border-radius: 50%;
}

.creatorName {
	margin-left: 10px;

	color: ${(props) => props.theme.colors.textWhiteColor};
	text-shadow: -1px 1px 7px #EB4040;

	font-size: 22px;
}

.videoText {
	display: flex;
	align-items: center;
	flex: 1;

	margin-bottom: 10px;
	padding: 0 10px;

	font-size: 18px;
	font-weight: 300;
	line-height: 22px;
}

.likes,
.comments,
.videoText {
	padding: 0 10px;

	color: ${(props) => props.theme.colors.textWhiteColor};

	font-size: 20px;
}
.comments {
	margin-bottom: 8px;
}

@media (max-width: 576px) {
	.cards {
		align-items: center;
		flex-direction: column;
	}

	.videoCard:not(:first-child) {
		margin-top: 20px;
	}
}
`;
