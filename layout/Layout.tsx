import React, { Dispatch, FunctionComponent } from "react";
import styled from "styled-components";

import { LayoutProps } from "../domain/Layout.props";
import { UserContextProvider } from "../store/userContext";
import { FeedsContextProvider } from "../store/feedsContext";

import Footer from "./Footer/Footer";
import Header from "./Header/Header";
import Main from "./Main/Main";

import { AppThemes } from "../store/themeContext";

export function Layout({ children, appTheme, setAppTheme }: LayoutProps): JSX.Element {
  return (
    <StyledAppLayout>
      <Header appTheme={appTheme} setAppTheme={setAppTheme} />
      <Main>{children}</Main>
      <Footer />
    </StyledAppLayout>
  );
}

export const withLayout = <T extends Record<string, unknown>>(Component: FunctionComponent<T>) => {
  // eslint-disable-next-line react/display-name
  return (props: T): JSX.Element => {
    return (
      <FeedsContextProvider data={props.data as []} error={props.error}>
        <UserContextProvider userFeed={props.userFeed} userInfo={props.userInfo} error={props.error}>
          <Layout
            appTheme={props.appTheme as AppThemes}
            setAppTheme={props.setAppTheme as Dispatch<React.SetStateAction<AppThemes>>}
          >
            <Component {...props} />
          </Layout>
        </UserContextProvider>
      </FeedsContextProvider>
    );
  };
};

const StyledAppLayout = styled.div`
  display: flex;
  overflow: hidden;
  flex-direction: column;
  justify-content: space-between;

  min-height: 100vh;
`;
