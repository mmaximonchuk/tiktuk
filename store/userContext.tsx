import React, { createContext, PropsWithChildren, useEffect, useState } from 'react';
import { iCustomError } from '../domain/HomePage.interface';
import { iUser, iUserFeed } from '../domain/User.interface';
export interface IUserContext {
	user: iUser | null;
	error: iCustomError | null | undefined;
	userFeed: iUserFeed | null;
	setUser: (user: iUser) => void;
}

export const UserContext = createContext<IUserContext>({ user: null, setUser: () => { }, userFeed: null, error: null });

export const UserContextProvider = ({ userFeed, userInfo, children, error }): JSX.Element => {
	const [user, setUser] = useState<iUser | null>(userInfo);

	return <UserContext.Provider value={{ user, setUser, userFeed, error }}>{ children }</UserContext.Provider>
};
