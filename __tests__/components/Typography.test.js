import React from "react";
import { ThemeProvider } from "styled-components";

import { render } from "../test-utils";
import { Typography } from '../../components';
import { TypographyTags } from '../../domain/Typography.props';

import { myDefaultTheme } from "../../styles/theme";

describe("Typography", () => {
  it("should render the p tag with className of p", () => {
    const text = "p tag rendered";

    const { getByText } = render(<ThemeProvider theme={myDefaultTheme}><Typography tag={TypographyTags.p}>{text}</Typography></ThemeProvider>);
    const pElement = getByText(text);

    expect(pElement).toBeVisible();
  });

  it("should render the span tag with className of span", () => {
    const text = "span tag rendered";

    const { getByText } = render(<ThemeProvider theme={myDefaultTheme}><Typography tag={TypographyTags.span}>{text}</Typography></ThemeProvider>);
    const spanElement = getByText(text);

    expect(spanElement).toBeVisible();
  });
});
