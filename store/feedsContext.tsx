import React, { createContext, PropsWithChildren, useEffect, useState } from "react";
import { iCustomError, iVideo } from "../domain/HomePage.interface";
import { NextRouter } from "next/router";

export interface IFeedsContext {
  data: iVideo[] | [];
  setVideoData: React.Dispatch<React.SetStateAction<iVideo[]>>;
  errorMessage: iCustomError | null | undefined;
  setErrorMessage: React.Dispatch<React.SetStateAction<iCustomError>>;
  currentPage: string | string[];
  setSetCurrentPage: React.Dispatch<React.SetStateAction<string | string[]>>;
  isFetching: boolean;
  setIsFetching: React.Dispatch<React.SetStateAction<boolean>>;
  handlePaginate: (page: number, router: NextRouter) => void;
}

export const FeedsContext = createContext<IFeedsContext>({
  data: [],
  setVideoData: () => {},
  errorMessage: null,
  setErrorMessage: () => {},
  currentPage: "1",
  setSetCurrentPage: () => {},
  isFetching: false,
  setIsFetching: () => {},
  handlePaginate: () => {},
});

export const FeedsContextProvider = ({ data = [] as iVideo[] | [], error, children }): JSX.Element => {
  const [videoData, setVideoData] = useState<iVideo[]>(data);
  const [errorMessage, setErrorMessage] = useState<iCustomError | undefined | null>(error);
  const [currentPage, setSetCurrentPage] = useState<string | string[]>("1");
  const [isFetching, setIsFetching] = useState<boolean>(false);

  useEffect(() => {
    // console.log("videoData", videoData);
  }, [videoData]);

  const handlePaginate = (page: number, router: NextRouter): void => {
    router.push(`?page=${page}`);
  };

  return (
    <FeedsContext.Provider
      value={{
        data: videoData,
        setVideoData,
        errorMessage,
        setErrorMessage,
        currentPage,
        setSetCurrentPage,
        isFetching,
        setIsFetching,
        handlePaginate,
      }}
    >
      {children}
    </FeedsContext.Provider>
  );
};
