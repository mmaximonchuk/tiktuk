import React, { useContext } from "react";
// import Image from "next/image";
import NavLink from "next/link";
import styled from 'styled-components';

import { Routes } from '../../helpers/routes';
import { Typography } from "../../components";
import { AppThemes, ThemeContext } from '../../store/themeContext';
import { TypographyTags } from '../../domain/Typography.props';
import { HeaderProps } from "../../domain/Header.props";

// import iconLogo from "../../assets/images/homePage/tik-tok.png";

// import s from "./Header.module.scss";

export default function Header(props: HeaderProps): JSX.Element {
  const {appTheme, setAppTheme} = props;
//   const { theme, setTheme } = useContext(ThemeContext);

  const toggleTheme = () => {
    setAppTheme(appTheme === AppThemes.Light ? AppThemes.Dark : AppThemes.Light)
  }
  return (
    <StyledHeader>
      <div className={'headerInner'}>
        <NavLink href={Routes.HomePage}>
          <a className={'logo'}>
            <img src={"../../assets/images/homePage/tik-tok.png"} alt="TikTuk" width={60} height={60} />
            {/* <Image src={iconLogo} alt="TikTuk" width="60" height="60" /> */}
            <Typography tag={TypographyTags.p} className={'logoText'}>
              TikTuk :
            </Typography>
          </a>
        </NavLink>
        <label className={'themeButtonContainer'}>
          <Typography className={'themeName'} tag={TypographyTags.span}>
            {appTheme === AppThemes.Light ? "Light" : "Dark"} mode
          </Typography>
          <input className={'themeCheckbox'} type="checkbox" checked={appTheme === AppThemes.Light ? true : false} onChange={toggleTheme} />
        </label>
      </div>
    </StyledHeader>
  );
}

const StyledHeader = styled.header`
	height: 80px;

	background-color: ${props => props.theme.colors.primary};
	box-shadow: 0 4px 4px ${props => props.theme.colors.black025};

  
.headerInner {
	display: flex;
	align-items: center;
	justify-content: flex-start;

	max-width: 1231px;
	height: 100%;
	margin: 0 auto;
	padding: 0 63px;
}

.logoText {
	margin-left: 18px;

	color: ${props => props.theme.colors.textWhiteColor};

	font-size: 24px;

	font-weight: 700;
	font-style: 21px;
}

.logo {
	display: flex;
	align-items: center;
}

.themeButtonContainer {
	margin-left: auto;

	cursor: pointer;
}

.themeName {
	user-select: none;

	color: ${props => props.theme.colors.textWhiteColor};

	font-size: 22px;
}

.themeCheckbox {
	width: 20px;
	height: 20px;
	position: absolute;
	visibility: hidden;
}

@media (max-width: 975px) {
	.headerInner {
		padding: 0 10px;
	}
}

`
