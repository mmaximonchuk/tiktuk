// import { DetailedHTMLProps, HTMLAttributes } from "react";

export interface TypographyProps {
  tag: TypographyTags;
  className?: string;
}

export enum TypographyTags {
  p = "p",
  span = "span",
}
